# window.py
#
# Copyright 2020 Leonard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from gi.repository import Gtk, Handy


class HeaderbarbugWindow(Handy.ApplicationWindow):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Handy.init()
        self.headerbar = Handy.HeaderBar(title="Hello World", show_close_button=True)
        self.add(self.headerbar)
        self.show_all()
